//
//  SkillVC.swift
//  app-swoosh
//
//  Created by Jake Christopher on 4/8/17.
//  Copyright © 2017 Jake Christopher Attard. All rights reserved.
//

import UIKit

class SkillVC: UIViewController {
    
    var player: Player!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(player.desiredLeague)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
