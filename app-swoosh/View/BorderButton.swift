//
//  BorderButton.swift
//  app-swoosh
//
//  Created by Jake Christopher on 11/7/17.
//  Copyright © 2017 Jake Christopher Attard. All rights reserved.
//

import UIKit

class BorderButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 2.0
        layer.borderColor = UIColor.white.cgColor
    }

}
