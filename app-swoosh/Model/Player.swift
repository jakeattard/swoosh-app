//
//  Player.swift
//  app-swoosh
//
//  Created by Jake Christopher on 4/8/17.
//  Copyright © 2017 Jake Christopher Attard. All rights reserved.
//

import Foundation

struct Player {
    var desiredLeague: String?
    var selectedSkillLevel: String?
}
